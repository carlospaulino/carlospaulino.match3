package com.carlospaulino.match3.managers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import com.carlospaulino.match3.Constants;
import com.carlospaulino.match3.R;

/**
 * Displays a dialog that asks the user for the size of the board and the amount of element types.
 * This class was created so we can recycle the alert dialog
 * Created by Carlos Paulino on 12/29/13.
 */
public class OptionsManager {
    private Context context;
    private boolean showCancelButton = true;

    public OptionsManager(Context context) {
        this.context = context;
    }

    public boolean isShowCancelButton() {
        return showCancelButton;
    }

    public void setShowCancelButton(boolean showCancelButton) {
        this.showCancelButton = showCancelButton;
    }

    /**
     *
     * @param onSelectionCompleted
     */
    public void show(final OnSelectionCompleted onSelectionCompleted) {
        // Inflate main view
        View optionsView = LayoutInflater.from(context).inflate(R.layout.options_dialog, null);

        // Board Size Spinner
        final Spinner spinnerBoardSize = (Spinner) optionsView.findViewById(R.id.spinnerBoardSize);

        ArrayAdapter<CharSequence> boardSizeAdapter = ArrayAdapter.createFromResource(
                context,
                R.array.board_sizes,
                android.R.layout.simple_spinner_item);
        boardSizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerBoardSize.setAdapter(boardSizeAdapter);
        // Set default selection
        spinnerBoardSize.setSelection(0);

        // Number of Block Types Spinner
        final Spinner spinnerNumberOfBlockTypes = (Spinner) optionsView.findViewById(R.id.spinnerNumberOfBlockTypes);

        ArrayAdapter<CharSequence> numberOfBlockTypesAdapter = ArrayAdapter.createFromResource(
                context,
                R.array.number_of_types,
                android.R.layout.simple_spinner_item);
        numberOfBlockTypesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerNumberOfBlockTypes.setAdapter(numberOfBlockTypesAdapter);

        // Set default selection
        spinnerNumberOfBlockTypes.setSelection(0);

        // Prepare alert dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(R.string.select_options)
                .setView(optionsView)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (onSelectionCompleted != null) {
                            onSelectionCompleted.selectionCompleted(
                                    spinnerBoardSize.getSelectedItemPosition() + Constants.DEFAULT_BOARD_SIZE,
                                    spinnerNumberOfBlockTypes.getSelectedItemPosition() + Constants.DEFAULT_BLOCK_TYPES
                            );
                        }
                    }
                });

        // We made the cancel button, because on the GameActivity it wasn't necessary to show it
        if (showCancelButton) {
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Nothing to here, as the dialog is automatically dismissed
                }
            });
        }

        builder.show().setCanceledOnTouchOutside(false);

    }

    /**
     * Simple interface to use as a callback
     */
    public interface OnSelectionCompleted {
        public void selectionCompleted(int boardSize, int numberOfBlockTypes);
    }
}
