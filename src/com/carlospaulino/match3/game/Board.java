package com.carlospaulino.match3.game;

import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import com.carlospaulino.match3.Constants;

import java.util.Random;

/**
 * Created by Carlos Paulino on 12/28/13.
 */
public class Board {
    protected Block[][] Blocks;
    protected int width;
    protected int height;
    protected int numberOfBlockTypes;
    protected Random random;

    /**
     * Creates a new instance of the board, and randomly generates a new board.
     *
     * @param width
     * @param height
     * @param numberOfBlockTypes
     */
    public Board(int width, int height, int numberOfBlockTypes) {
        this.width = width;
        this.height = height;
        this.numberOfBlockTypes = numberOfBlockTypes;
        this.Blocks = new Block[width][height];
        this.random = new Random();

        generateBoard();

    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /**
     * Returns the current block
     *
     * @return
     */
    public Block[][] getBlocks() {
        return Blocks;
    }

    /**
     * Iterates over the board and generate a block with a random type
     */
    protected void generateBoard() {

        for (int x = 0; x < width; x++) {

            for (int y = 0; y < height; y++) {

                Blocks[x][y] = new Block(getRandomBlockType());

            }
        }
    }

    /**
     * Returns the board in html format so it can be drawn
     * inside a textview. This is only useful for development
     * porposes
     *
     * @return
     */
    public Spanned printBoard() {
        StringBuilder sb = new StringBuilder();

        for (int x = 0; x < width; x++) {

            for (int y = 0; y < height; y++) {

                sb.append(Blocks[x][y].toString());

                if (y < height - 1) {
                    sb.append(" | ");
                }
            }
            sb.append("<br />");

        }
        return Html.fromHtml(sb.toString());
    }

    /**
     * Finds the Matches. Horizontal(true) or Vertical(false)
     */
    public void findMatches(boolean direction) {
    // Flip based on the direction. Horizontal or Vertical
        for (int a = 0; a < width; a++) {

            for (int b = 0; b < width; b++) {

                Block currentBlock = direction ? Blocks[a][b] : Blocks[b][a];

                // Matched blocks are ignored
                if (currentBlock.getStatus() == Block.STATUS_MATCHED) continue;

                int matchedCount = 1;

                /**
                 * We start comparing from my current position to the right or to the bottom
                 */

                for (int i = b + 1; i < height; i++) {
                    Block blockToCompare = direction ? Blocks[a][i] : Blocks[i][a];
                    // Only if we have the same type and next one isn't matched
                    if (currentBlock.getType() == blockToCompare.getType() && blockToCompare.getStatus() != Block.STATUS_MATCHED) {
                        matchedCount++;
                    } else {

                        // Move on
                        break;

                    }
                }

                // Per the specs we defined the minimum matches to three
                if (matchedCount >= Constants.MINIMUM_MATCHES) {

                    // Start preparing the text that we need to print to logcat
                    StringBuilder sb = new StringBuilder();
                    sb.append(String.format("Match %d (%d):", matchedCount, currentBlock.getType()));

                    for (int i = b; i < (b + matchedCount); i++) {

                        if (direction) {
                            Blocks[a][i].setMatched(matchedCount);

                            sb.append(String.format("{ %d , %d }", a, i));
                        } else {

                            Blocks[i][a].setMatched(matchedCount);
                            sb.append(String.format("{ %d , %d }", i, a));
                        }
                    }
                    // Print to log vat
                    Log.i(Constants.DEBUG_TAG, sb.toString());
                }
            }

        }

    }

    /**
     * Find all the matches in the board
     */
    public void findMatches() {
        findMatches(true);
        findMatches(false);
    }

    /**
     * Iterates over the entire board and changes the status from Matched to Deleted
     */
    public void markedDeleted() {
        for (int x = 0; x < width; x++) {

            for (int y = 0; y < height; y++) {

                Block currentBlock = Blocks[x][y];

                if (currentBlock.getStatus() == Block.STATUS_MATCHED) {

                    currentBlock.setStatus(Block.STATUS_DELETED);

                }
            }
        }
    }

    /**
     * Generates a random number between 1 and the numberOfBlockTypes
     *
     * @return
     */
    protected int getRandomBlockType() {
        return random.nextInt(numberOfBlockTypes) + 1;
    }
}
