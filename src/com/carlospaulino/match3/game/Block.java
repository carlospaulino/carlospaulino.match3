package com.carlospaulino.match3.game;

import com.carlospaulino.match3.Constants;

/**
 * Created by Carlos Paulino on 12/28/13.
 */
public class Block {
    public static int STATUS_NONE = 0;
    public static int STATUS_MATCHED = 1;
    public static int STATUS_DELETED = 2;

    /**
     * This is just the number that we display on the block
     */
    private int type;

    /**
     We save the number of matching blocks so
     we can can now what color to use
     when changing the background
     */
    private int matchCount;

    /**
     Holds the status of the block. It could either be
     matched or deleted
      */
    private int status;

    public Block(int type) {
        this.type = type;
    }

    public int getMatchCount() {
        return matchCount;
    }

    public void setMatchCount(int matchCount) {
        this.matchCount = matchCount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * Set the status to STATUS_MATCHED and the amount of blocks
     * that match with this one
     * @param matchCount
     */
    public void setMatched(int matchCount) {
        this.status = STATUS_MATCHED;
        this.matchCount = matchCount;
    }


    /**
     * Draws the board in plaintext. Used for debugging purposes.
     * @return
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("<font color=\"");
        if (status == STATUS_MATCHED) {
            sb.append(Constants.COLORS[getMatchCount()]);
        } else {
            sb.append("#000000");
        }
        sb.append("\">");

        if (status == STATUS_DELETED) {
            sb.append("&nbsp;+&nbsp;");
        } else {
            if (getType() < 10) {
                sb.append("&nbsp;");
            }
            sb.append(getType());
            if (getType() < 10) {
                sb.append("&nbsp;");
            }
        }
        sb.append("</font>");

        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Block) {
            return ((Block) o).getType() == getType();
        }
        return false;
    }

    public int getType() {
        return this.type;
    }

}
