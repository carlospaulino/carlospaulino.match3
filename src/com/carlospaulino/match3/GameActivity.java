package com.carlospaulino.match3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import com.carlospaulino.match3.game.Block;
import com.carlospaulino.match3.game.Board;
import com.carlospaulino.match3.managers.OptionsManager;

/**
 * Created by Carlos Paulino on 12/28/13.
 */
public class GameActivity extends Activity {
    Button buttonCancel;
    Button buttonNext;
    Board gameBoard;
    GridView gridViewBoard;
    GridViewAdapter adapter;

    /** Note:
     * 0 : Is Default Board
     * 1 : Is Marked Board
     * 2 : Is Deleted Board
     */
    int gameState = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);

        /*
        Setup views. Probably I should have used a dependency injection library but I didn't want
        to use external libraries in this project
         */
        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonNext = (Button) findViewById(R.id.buttonNext);
        gridViewBoard = (GridView) findViewById(R.id.gridViewBoard);

        int boardSize = getIntent().getIntExtra(Constants.BOARD_SIZE, Constants.DEFAULT_BOARD_SIZE);

        int numberOfBlockTypes = getIntent().getIntExtra(Constants.NUMBER_OF_BLOCK_TYPES, Constants.DEFAULT_BLOCK_TYPES);

        initGame(boardSize, numberOfBlockTypes);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (gameState) {
                    case 0:
                        gameBoard.findMatches();
//                      textBoard.setText(gameBoard.printBoard());
                        buttonNext.setText(R.string.mark_deleted);
                        adapter.notifyDataSetChanged();
                        break;
                    case 1:
                        buttonNext.setText(R.string.retry);
                        gameBoard.markedDeleted();
                        adapter.notifyDataSetChanged();
//                        textBoard.setText(gameBoard.printBoard());
                        break;
                    case 2:
                        confirmRestart();
                        break;
                }

                gameState++;
            }
        });
    }


    /**
     * Creates a new board based on the dimensions and number of block types.
     * Also setups the custom array adapter and the gridview
     * @param boardSize
     * @param numberOfBlockTypes
     */
    private void initGame(int boardSize, int numberOfBlockTypes) {

        gameBoard = new Board(boardSize, boardSize, numberOfBlockTypes);

        adapter = new GridViewAdapter();
        gridViewBoard.setAdapter(adapter);
        gridViewBoard.setNumColumns(boardSize);
    }

    /**
     * Display a dialog to confirm that the user wants to play again
     */
    private void confirmRestart() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.hey_you)
                .setMessage(R.string.do_you_want_to_play_again)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        restartGame();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
    }

    /**
     * Resets the game state and ask for options for the new board
     */
    private void restartGame() {
        OptionsManager o = new OptionsManager(this);
        o.setShowCancelButton(false);
        o.show(new OptionsManager.OnSelectionCompleted() {
            @Override
            public void selectionCompleted(int boardSize, int numberOfBlockTypes) {
                initGame(boardSize, numberOfBlockTypes);
            }
        });
        // Reset button text to the initial value
        buttonNext.setText(R.string.find_matches);
        gameState = 0;

    }

    /**
     * This is our custom Array Adapter
     */
    protected class GridViewAdapter extends ArrayAdapter {
        public GridViewAdapter() {
            super(GameActivity.this, 0);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            // We translate from a sequential position to coordinates
            int x = position / gameBoard.getWidth();
            int y = position % gameBoard.getWidth();

            // Inflate views
            View view = getLayoutInflater().inflate(R.layout.grid_cell, null);

            Button cellText = (Button) view.findViewById(R.id.cellText);

            // Hold the block that we are working on
            Block block = gameBoard.getBlocks()[x][y];

            if (block.getStatus() == Block.STATUS_DELETED) {
                cellText.setBackgroundColor(Color.WHITE);
                cellText.setText("X");
            } else {

                cellText.setText(String.valueOf(block.getType()));

                /*
                 If the status changed to Matched then we change the background color of the view.
                 Since we are currently working with a max sequence of 12 elements we take colors from
                 our Colors constants
                  */

                if (block.getStatus() == Block.STATUS_MATCHED) {
                    cellText.setBackgroundColor(Color.parseColor(Constants.COLORS[block.getMatchCount()]));
                }

            }

            return view;
        }


        /**
         * Its important that this method was overriden because
         * we didn't pass any collections to the constructor
         * @return
         */
        @Override
        public int getCount() {
            /*
             Since the GridView Widget displays items sequentially we
             multiply the dimensions of the array in order to get to total amount
             of elements
              */
            return gameBoard.getHeight() * gameBoard.getWidth();
        }
    }
}
