package com.carlospaulino.match3;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import com.carlospaulino.match3.managers.OptionsManager;

public class StartActivity extends Activity {
    Button startGameButton;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        startGameButton = (Button) findViewById(R.id.startGameButton);

        startGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayDialog();
            }
        });
    }


    /**
     * Display the options dialog so we can get the board parameters
     */
    private void displayDialog(){
        new OptionsManager(this).show(new OptionsManager.OnSelectionCompleted() {
            @Override
            public void selectionCompleted(int boardSize, int numberOfBlockTypes) {

                Intent intent = new Intent(StartActivity.this, GameActivity.class);
                intent.putExtra(Constants.BOARD_SIZE, boardSize);
                intent.putExtra(Constants.NUMBER_OF_BLOCK_TYPES, numberOfBlockTypes);
                startActivity(intent);
            }
        });
    }
}
