package com.carlospaulino.match3;

/**
 * Created by Carlos Paulino on 12/28/13.
 */
public class Constants {
    public static final boolean DEBUG = true;
    public static final String DEBUG_TAG = "CP_MATCH3";
    public static final String BOARD_SIZE = "board_size";
    public static final String NUMBER_OF_BLOCK_TYPES = "number_of_block_types";
    public static final int DEFAULT_BOARD_SIZE = 5;
    public static final int DEFAULT_BLOCK_TYPES = 3;
    public static final int MINIMUM_MATCHES = 3;
    public static final String[] COLORS = new String[] {
            "#FF3366",
            "#FFCC33",
            "#B88A00",
            "#33CCFF",
            "#3366FF",
            "#FFFF00",
            "#00FF00",
            "#666666",
            "#FF0A0A",
            "#123123",
            "#0000FF",
            "#00FFFF",
    };
}
